
<!DOCTYPE>

<html>
<head>
  <meta charset="UTF-8" />
  <title>新規会員登録</title>
</head>
<body>
  <h1>project one</h1>
  <form action="" method="post">
    <dl>
      <dt>ユーザー名</dt>
      <dd>
        <?php
        require("CreateUser.php");
        echo htmlspecialchars($_SESSION["name"], ENT_QUOTES, 'UTF-8'); ?>
      </dd>
      <dt>メールアドレス</dt>
      <dd>
        <?php echo htmlspecialchars($_SESSION["mail"], ENT_QUOTES, 'UTF-8'); ?>
      </dd>
      <dt>パスワード</dt>
      <dd>
        【表示されません】
      </dd>
    </dl>
    <div><a href="CreateUser.php?action=rewrite">&laquo;&nbsp;書き直す</a>
      <input type="submit" value="登録する" id="regist"></div>
    </form>
  </body>
  </html>
