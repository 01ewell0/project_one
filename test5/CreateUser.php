<?php
// セッション開始
session_start();

// エラーメッセージの初期化
$errorMessage = "";

$CompleteMessage="";

// ログインボタンが押された場合
if (isset($_POST["next"])) {
  // １．ユーザIDの入力チェック
	if (empty($_POST["userid"])) {
		$errorMessage = "ユーザIDが未入力です。";
	} else if (empty($_POST["password"])) {
		$errorMessage = "パスワードが未入力です。";
	}else if (empty($_POST["name"])) {
		$errorMessage = "名前が未入力です。";
	}else if (empty($_POST["mail"])) {
			$errorMessage = "メールアドレスが未入力です。";
	}
  // ２．ユーザIDとパスワードが入力されていたら認証する
	if (!empty($_POST["userid"]) && !empty($_POST["password"])&& !empty($_POST["mail"])&& !empty($_POST["name"])) {

		$_SESSION["USERID"]=$_POST["userid"];
		$_SESSION["NAME"]=$_POST["name"];
		$_SESSION["MAIL"]=$_POST["mail"];
		$_SESSION["PASS"]=$_POST["password"];

		$CompleteMessage='登録が完了しました。';

		header("Location: check.php");
		exit();
	} else {
    // 未入力なら何もしない
	}
}

 ?>

 <!DOCTYPE>


 <html>
 <head id="a">
 	<link rel="stylesheet" type="text/css" href="css/style_CU.css">
 	<meta charset="UTF-8">
 	<title>Login Page</title>

 </head>

 <body id="login">
 	<div id="header">
 		<img src="images/logo2.png" id="image">
 	</div>

  <div id="form">
    <!-- $_SERVER['PHP_SELF']はXSSの危険性があるので、actionは空にしておく -->
    <!--<form id="loginForm" name="loginForm" action="<?php print($_SERVER['PHP_SELF']) ?>" method="POST">-->
    <form id="loginForm" name="loginForm" action="" method="POST">
      <div id="sqlogin">
				<div id=inform>
        <label id="ll">ユーザ登録フォーム</label><br>
				<label id="ms2">登録後は確認のため指定アドレスにメールします</label>
        <div id="error"><?php echo $errorMessage ?></div>
        <div id="new">
        <label for="userid">User ID:</label>
        <input type="text" id="userid" name="userid" value="">
        <label id="ms">[※半角数字１０字以内]</label>
        <br>
        <label for="password">Password:</label>
        <input type="password" id="password" name="password" value="">
        <label id="ms">[※半角英数２０字以内]</label>
        <br>
        <label for="name">Name:</label>
        <input type="name" id="name" name="name" value="">
        <label id="ms">[※半角英数字５０字以内]</label>
        <br>
        <label for="mail">Mail:</label>
        <input type="mail" id="mail" name="mail" value="">
        <label id="ms">[※半角英数字５０字以内]</label>
        <br>

        <input type="submit" id="button" name="next" value="確認" onClick="form.action='check.php'>
				<div id="cm"><?php echo $CompleteMessage ?></div>
      </div>
		</div>
      </div>

    </form>
  </div>

 			</form>
 		</div>
 	</body>
 	</html>
</html>
