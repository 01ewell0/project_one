<?php
  session_start();
  $userid=$_SESSION["USERID"];
  $name=$_SESSION["NAME"];
  $mail=$_SESSION["MAIL"];
  $pass=$_SESSION["PASS"];


  if(isset($_POST["create"])){
    $mysqli = new mysqli('localhost', 'root', 'pass');
		if ($mysqli->connect_errno) {
			print('<p>データベースへの接続に失敗しました。</p>' . $mysqli->connect_error);
			exit();
		}
    // データベースの選択
		$mysqli->select_db('db_user');
		$mysqli->query("set names utf8");

    // クエリの実行
		$query = "INSERT INTO users (id,name,password,mail) values (".$userid.",'".$name."','".$pass."','".$mail."')";
		$result = $mysqli->query($query);
		if (!$result) {
			print('クエリーが失敗しました。' . $mysqli->error);
			$mysqli->close();
			exit();
		}
    mb_language("japanese");
    mb_internal_encoding("UTF-8");
    //日本語メール送信
		$to = $mail;
		$subject = "会員登録完了のお知らせ";
		$body = "会員登録が以下の内容で完了したことをおしらせします
		ユーザID:".$userid."
		メールアドレス：".$mail."
		お名前：".$name."
		パスワード：".$pass."
		この度はご登録ありがとうございます。";
		$from = mb_encode_mimeheader(mb_convert_encoding("Project One!","UTF-8","EUC-JP"))."<project-one@example.com>";

		//ちゃんと日本語メールが送信できます
		mb_send_mail($to,$subject,$body,"From:".$from);

    header("Location: checkall.php");
    exit();
  }
  if(isset($_POST["back1"])){
    header("Location: CreateUser.php");
    exit();
  }
 ?>

 <!DOCTYPE>
 <html>
 <head id="a">
  <link rel="stylesheet" type="text/css" href="css/kakunin.css">
  <meta charset="UTF-8">
  <title>Login Page</title>

 </head>

 <body id="login">
  <div id="header">
    <img src="images/logo2.png" id="image">
  </div>

  <div id="form">
    <!-- $_SERVER['PHP_SELF']はXSSの危険性があるので、actionは空にしておく -->
    <!--<form id="loginForm" name="loginForm" action="<?php print($_SERVER['PHP_SELF']) ?>" method="POST">-->
    <form id="loginForm" name="loginForm" action="" method="POST">
      <div id="sqlogin">

        <label id="ll">登録確認</label><br>
        <div id="kakunin">
        <label id="l1">User ID:</label>
        <label id="l2"><?php echo $userid ?></label>
        <br>
        <label id="l1">Password:</label>
        <label id="l2">[表示されません]</label>
        <br>

        <label id="l1">Name:</label>
        <label id="l2"><?php echo $name ?></label>
        <br>

        <label id="l1">Mail:</label>
        <label id="l2"><?php echo $mail ?></label>
        <br>

        <input type="submit" id="button" name="create" value="作成">
        <input type="submit" id="bbutton" name="back1" value="戻る">


        <div id="cm"><?php echo $CompleteMessage ?></div>
      </div>
    </div>
      </div>

    </form>
  </div>

      </form>
    </div>
  </body>
  </html>
 </html>
