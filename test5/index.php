<?php
require 'password.php';
// セッション開始
session_start();

// エラーメッセージの初期化
$errorMessage = "";

// ログインボタンが押された場合
if (isset($_POST["login"])) {
  // １．ユーザIDの入力チェック
	if (empty($_POST["userid"])) {
		$errorMessage = "ユーザIDが未入力です。";
	} else if (empty($_POST["password"])) {
		$errorMessage = "パスワードが未入力です。";
	}

  // ２．ユーザIDとパスワードが入力されていたら認証する
	if (!empty($_POST["userid"]) && !empty($_POST["password"])) {
    // mysqlへの接続
		$mysqli = new mysqli('localhost', 'root', 'pass');
		if ($mysqli->connect_errno) {
			print('<p>データベースへの接続に失敗しました。</p>' . $mysqli->connect_error);
			exit();
		}

    // データベースの選択
		$mysqli->select_db('db_user');
		$mysqli->query("set names utf8");

    // 入力値のサニタイズ
		$userid = $mysqli->real_escape_string($_POST["userid"]);

    // クエリの実行
		$query = "SELECT * FROM users WHERE id = '" . $userid . "'";
		$result = $mysqli->query($query);
		if (!$result) {
			print('クエリーが失敗しました。' . $mysqli->error);
			$mysqli->close();
			exit();
		}
		$row =  $result->fetch_assoc();
		$db_hashed_pwd=$row['password'];

    // データベースの切断
		$mysqli->close();

    // ３．画面から入力されたパスワードとデータベースから取得したパスワードのハッシュを比較します。
		if ($_POST["password"] == $db_hashed_pwd) {

    //if (password_verify($_POST["password"], $db_hashed_pwd)) {
      // ４．認証成功なら、セッションIDを新規に発行する
			session_regenerate_id(true);
			$_SESSION["USERID"] = $_POST["userid"];
			$_SESSION["NAME"]= $row['name'];
			header("Location: data.php");
			exit;
		}
		else {
      // 認証失敗
			$errorMessage = "ユーザIDあるいはパスワードに誤りがあります。";
		}
	} else {
    // 未入力なら何もしない
	}
}

?>


<!DOCTYPE>
<html>
<head id="a">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="index.php"></script>
	<meta charset="UTF-8">
	<title>Login Page</title>

</head>

<body id="login">
	<div id="header">
		<img src="images/logo2.png" id="image">


	</div>

		<div id="form">
			<form id="loginForm" name="loginForm" action="" method="POST">
				<div id="sqlogin">
					<div id="user_in">
						<label for="userid">ID:</label>
						<input type="text" id="userid" name="userid" value="">
					</div>
					<br>
					<div id="pass_in">
						<label for="password">PASS:</label>
						<input type="password" id="password" name="password" value="">
					</div>
					<br>
					<div id="error"><?php echo $errorMessage ?></div>
					<INPUT type="submit" name="submit"id="button2" value="新規ユーザ登録" onClick="form.action='CreateUser.php'">
					<input type="submit" id="button" name="login" value="Login">

				</div>

			</form>
		</div>
	</body>
	</html>
