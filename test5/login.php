<?php
require 'password.php';
// セッション開始
session_start();

// エラーメッセージの初期化
$errorMessage = "";

// ログインボタンが押された場合
if (isset($_POST["login"])) {
  // １．ユーザIDの入力チェック
	if (empty($_POST["userid"])) {
		$errorMessage = "ユーザIDが未入力です。";
	} else if (empty($_POST["password"])) {
		$errorMessage = "パスワードが未入力です。";
	}

  // ２．ユーザIDとパスワードが入力されていたら認証する
	if (!empty($_POST["userid"]) && !empty($_POST["password"])) {
    // mysqlへの接続
		$mysqli = new mysqli('localhost', 'root', 'pass');
		if ($mysqli->connect_errno) {
			print('<p>データベースへの接続に失敗しました。</p>' . $mysqli->connect_error);
			exit();
		}

    // データベースの選択
		$mysqli->select_db('db_user');

    // 入力値のサニタイズ
		$userid = $mysqli->real_escape_string($_POST["userid"]);

    // クエリの実行
		$query = "SELECT * FROM new_user WHERE id = '" . $userid . "'";
		$result = $mysqli->query($query);
		if (!$result) {
			print('クエリーが失敗しました。' . $mysqli->error);
			$mysqli->close();
			exit();
		}
		$row =  $result->fetch_assoc();
		$db_hashed_pwd=$row['password'];

    // データベースの切断
		$mysqli->close();

    // ３．画面から入力されたパスワードとデータベースから取得したパスワードのハッシュを比較します。
		if ($_POST["password"] == $db_hashed_pwd) {

    //if (password_verify($_POST["password"], $db_hashed_pwd)) {
      // ４．認証成功なら、セッションIDを新規に発行する
			session_regenerate_id(true);
			$_SESSION["USERID"] = $_POST["userid"];
			$_SESSION["NAME"]= $row['name'];
			header("Location: main.php");
			exit;
		}
		else {
      // 認証失敗
			$errorMessage = "ユーザIDあるいはパスワードに誤りがあります。";
		}
	} else {
    // 未入力なら何もしない
	}
}

?>
