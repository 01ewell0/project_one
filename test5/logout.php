<?php
session_start();

if (isset($_SESSION["USERID"])) {
  $errorMessage = "ログアウトしました。";
}
else {
  $errorMessage = "セッションがタイムアウトしました。";
}
@session_destroy();
?>

<!DOCTYPE>
<html>
<head id="a">
  <link rel="stylesheet" type="text/css" href="css/style_data.css">
  <script type="text/javascript" src="login.php"></script>
  <meta charset="UTF-8">
  <title>Login Page</title>

</head>

<body id="login">
  <div id="header">
    <img src="images/logo2.png" id="image">
  </div>
  <div id="menubar">
    <ul>
      <li><a href="">---</a></li>
      <li><a href="#">---</a></li>
      <li><a href="#">---</a></li>
      <li><a href="index.php">ログイン画面に戻る</a></li>
    </ul>
  </div>
  <div id="me">
    <img src="images/out.png" id="tori">
  </div>
</body>
</html>
