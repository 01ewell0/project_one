<?php
session_start();

// ログイン状態のチェック
if (!isset($_SESSION["USERID"])) {
  header("Location: logout.php");

  exit;
}else{
  $mysqli = new mysqli('localhost', 'kit', 'pass');
  if ($mysqli->connect_errno) {
    print('<p>データベースへの接続に失敗しました。</p>' . $mysqli->connect_error);
    exit();
  }

  // データベースの選択
  $mysqli->select_db('db_user');

  // クエリの実行（個別に取得）
  $query1 = "SELECT hour FROM db_kion2";
  $query2 = "SELECT temperture FROM db_kion2";
  $result1 = $mysqli->query($query1);
  $result2 = $mysqli->query($query2);

  if (!$result1 || !$result2) {
    print('クエリーが失敗しました。' . $mysqli->error);
    $mysqli->close();
    exit();
  }
  //データベースから取得したデータは、配列に代入しjavascriptで用いる。
  $arr1 = array();
  $arr2 = array();
  $index=2;
  while ($row1 = mysqli_fetch_array($result1, MYSQL_NUM)) {
    $row2 = mysqli_fetch_array($result2, MYSQL_NUM);
    $arr1[]=$row1;
    $arr2[]=$row2;
  }

  //データベースを閉じる。
  $mysqli->close();
}

?>

<!doctype html>
<html>
<head>
  <script src="amcharts/amcharts.js" type="text/javascript"></script>
  <script src="amcharts/serial.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="style_main.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="bxslider/jquery.bxslider.min.js"></script>
  <link href="bxslider/jquery.bxslider.css" rel="stylesheet" />
  <meta charset="UTF-8">
  <title >農業日誌</title>
  <script type="text/javascript">
  $(document).ready(function(){
    $('.bxslider').bxSlider({
      auto: true,
    });
  });
  </script>
  <div id="globalnavi">
    <ul>
      <li><a href="login.php">Home</a></li>
      <li><a href="#">カレンダー</a></li>
      <li><a href="#">日誌</a></li>
      <li><a href="#">ブログ</a></li>
      <li><a href="logout.php">ログアウト</a></li>
    </ul>
  </div>
</head>
<body>



  <br>
  <div id="pagebody">
    <div id="header"><h1>農業日誌 Sample.ver</a></h1></div><br><br>
    <!-- ユーザIDにHTMLタグが含まれても良いようにエスケープする -->
    <p><?=htmlspecialchars($_SESSION["NAME"], ENT_QUOTES,"UTF-8"); ?>さんのマイページ</p>
    <h2>5/22の気温変化</h2>
    <div id="chartdiv" style="width: 800px; height: 200px;"></div>
    <script type="text/javascript">
    var dat1 = JSON.parse('<?=json_encode($arr1);?>');
    var dat2 = JSON.parse('<?=json_encode($arr2);?>');
    var data=[];
    for (var i=0;i<dat2.length;i++) {
      data[i]=[];
      data[i]["hour"]= dat1[i];
      //data[i]["date"]= i;
      data[i]["temperture"]= dat2[i];
    }

    //折れ線グラフを書く
    var chart;
    AmCharts.ready(function () {
      chart = new AmCharts.AmSerialChart();
      chart.dataProvider = data;

      chart.categoryField = "hour";
      //chart.dataDateFormat = "MM-DD";

      var balloon = chart.balloon;
      balloon.cornerRadius = 6;
      balloon.adjustBorderColor = false;
      balloon.horizontalPadding = 10;
      balloon.verticalPadding = 10;

      var durationGraph = new AmCharts.AmGraph();
      durationGraph.title = "temperture";
      durationGraph.valueField = "temperture";
      durationGraph.type = "line";
      durationGraph.fillColorsField = "lineColor";
      durationGraph.fillAlphas = 0.3;
      durationGraph.balloonText = "[[value]]℃";
      durationGraph.lineThickness = 1;
      durationGraph.legendValueText = "[[value]]";
      durationGraph.bullet = "square";
      durationGraph.bulletBorderThickness = 1;
      durationGraph.bulletBorderAlpha = 1;
      chart.addGraph(durationGraph);

      var chartCursor = new AmCharts.ChartCursor();
      chartCursor.zoomable = false;
      //chartCursor.categoryBalloonDateFormat = "YYYY MMM DD";
      chartCursor.cursorAlpha = 0;
      chart.addChartCursor(chartCursor);

      var chartScrollbar = new AmCharts.ChartScrollbar();
      chart.addChartScrollbar(chartScrollbar);

      chart.write("chartdiv");
    });
    </script>
    <div id="bx">
      <ul class="bxslider">
        <li><img title="1" alt="" src="images/016.JPG" width="300" height="300" /></li>
        <li><img title="2" alt="" src="images/018.JPG" width="300" height="300" /></li>
        <li><img title="3" alt="" src="images/DSCN2461.JPG" width="300" height="300" /></li>
        <li><img title="4" alt="" src="images/DSCN2456.JPG" width="300" height="300" /></li>
      </ul>
    </div>
  </div>


</body>
</html>
