<!DOCTYPE>
<html>
<head id="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="dygraph-combined.js"></script>
    <script type="text/javascript" src="dygraph-combined.js"></script>
  <link rel="stylesheet" type="text/css" href="css/style_data.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="main.css" >

  <meta charset="UTF-8">
  <title>日誌</title>

</head>

<body id="login">
  <div id="header">
    <img src="images/logo2.png" id="image">
  </div>
  <div id="menubar">
    <ul>
      <li><a href="data.php">Home</a></li>
      <li><a href="calender.php">カレンダから見る</a></li>
      <li><a href="nisshi.php">日誌をつける</a></li>
      <li><a href="logout.php">ログアウト</a></li>
    </ul>
  </div>
  <div id="main_body">
    <div id="user"><?php echo $user ?></div>
    <label>日誌ページ　仮</label>
  </div>
</body>
<br/>
<br/>



<div class="graph">
      <div id="graphdiv"></div>
      <div id="labels"></div>
      <div id="switch" style="clear:both; ">
      <input type="checkbox" id="0" checked="checked" onclick="change(this)" />Data1
      <input type="checkbox" id="1" checked="checked" onclick="change(this)" />Data2
      <input type="checkbox" id="2" checked="checked" onclick="change(this)" />Data3
      <input type="checkbox" id="3" checked="checked" onclick="change(this)" />Data4
      <input type="checkbox" id="4" checked="checked" onclick="change(this)" />Data5
      </div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
  // クリック時に実行されるcallback、ハイライトを固定する
  var makeClickCallback = function(graph) {
    var isLocked = false;
    return function(ev) {
      if (isLocked) {
        graph.clearSelection();
        isLocked = false;
      } else {
        graph.setSelection(graph.getSelection(), graph.getHighlightSeries(), true);
        isLocked = true;
      }
    };
  };

  // グラフ描画ロジック
  container = document.getElementById("graphdiv");
  labels = document.getElementById("labels");
  labels_arr = ['Date','Data1','Data2','Data3','Data4','Data5'];
  g = new Dygraph(
    container,
    "file.csv",
    {
       labels: labels_arr, // ラベル名指定
       labelsDiv: labels,  // ラベル表示位置
       showRangeSelector: true, // 表示範囲セレクターを表示
       labelsSeparateLines: true, // ラベルの後ろに改行挟む
       highlightSeriesOpts: {  // ハイライトの設定
         strokeWidth: 3,
         strokeBorderWidth: 1,
         highlightCircleSize: 5
       }
  });
  // クリック時のcallback指定
  g.updateOptions({clickCallback: makeClickCallback(g)}, true);

  // 表示非表示切り替え
  function change(el) {
    g.setVisibility(parseInt(el.id), el.checked);
  }
</script>
<form action="regist.php" method="post">
  名前：<br />
  <input type="text" name="name" size="15" value="" /><br />
  業務記録：<br />
  <textarea name="message" cols="60" rows="3"></textarea><br />
  <br />
  <input type="submit" value="記録する" />
</form>

<?php

$con = mysql_connect('localhost', 'root', 'pass');
if (!$con) {
  exit('データベースに接続できませんでした。');
}

$result = mysql_select_db('db_user', $con);
if (!$result) {
  exit('データベースを選択できませんでした。');
}

$result = mysql_query('SET NAMES utf8', $con);
if (!$result) {
  exit('文字コードを指定できませんでした。');
}

$result = mysql_query('SELECT * FROM messages ORDER BY no DESC', $con);
while ($data = mysql_fetch_array($result)) {
  echo "<p>\n";
  echo '<strong>[No.' . $data['no'] . '] ' . htmlspecialchars($data['name'], ENT_QUOTES) . ' ' . $data['created'] . "</strong><br />\n";
  //echo "<br />\n";
  echo nl2br(htmlspecialchars($data['message'], ENT_QUOTES));
  echo "</p>\n";
}

$con = mysql_close($con);
if (!$con) {
  exit('データベースとの接続を閉じられませんでした。');
}

?>


</body>

</html>
