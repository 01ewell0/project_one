<?php
header("Content-Type: text/html; charset=UTF-8");
$url = 'http://api.openweathermap.org/data/2.5/weather?q=hakusan,jp&units=metric';
$weather = json_decode(file_get_contents($url), true);
$weatherShow = '<div>天気:%s</div>
<div>天気詳細:%s</div>
<div><img src="http://openweathermap.org/img/w/%s.png" style="width:200px"></div>
<div>温度:%s 度</div>
<div>湿度:%s パーセント</div>
<div>風速:%sメートル</div>';

echo sprintf($weatherShow, $weather['weather'][0]['main'], $weather['weather'][0]['description'], $weather['weather'][0]['icon'], $weather['main']['temp'], $weather['main']['humidity'], $weather['wind']['speed']);
?>
